#include <FastLED.h>

#define LED_PIN 1
#define NUM_LEDS 36

CRGB leds[NUM_LEDS];

uint8_t dim_led[4];

void setup() {
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);

  for (int i = 0; i < (NUM_LEDS / 2); i++) {
    leds[i] = CRGB(255,255,255);
  }

  for (int i = (NUM_LEDS/2); i < NUM_LEDS; i++) {
    leds[i] = CRGB(255,0,0);
  }

  dim_led[0] = 0;
  dim_led[1] = 1;

  dim_led[2] = 18;
  dim_led[3] = 19;

  FastLED.show();
}

void loop() {

  CRGB dim[4];
  for (int i=0; i<4; i++) {
    dim[i].red = leds[dim_led[i]].red;
    dim[i].green = leds[dim_led[i]].green;
    dim[i].blue = leds[dim_led[i]].blue;

    leds[dim_led[i]].red = dim[i].red / 10;
    leds[dim_led[i]].green = dim[i].green / 10;
    leds[dim_led[i]].blue = dim[i].blue / 10;
  }

  FastLED.show();

  delay(50);

  for (int i=0; i<4; i++) {
    leds[dim_led[i]].red = dim[i].red;
    leds[dim_led[i]].green = dim[i].green;
    leds[dim_led[i]].blue = dim[i].blue;

    dim_led[i] += 1;
    if (dim_led[i] >= NUM_LEDS) {
      dim_led[i] = 0;
    }
  }
}
