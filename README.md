![](doc/images/tour.jpg)

\-			   | \-
:-------------------------:|:-------------------------:
![](doc/images/night.jpg)  |  ![](doc/images/off.jpg)

# ridesafe flag

This project is mainly intended to increase visibility for recumbent
bi-/tricycles and velomobiles while still keeping aerodynamic drag as low as
possible.
I recently discovered  WS2812B LEDs which appeared to be quite bright.
Using this setup the LED flag is bright enough to also illuminate the street
around the bike a little. 
The flag should be visible from all sides, therefore some part was required to
scatter the light into all directions. It turned out that printing transparent
filament is a great way to achieve this kind of light scattering.

## Parts needed

- 3D printer
- [transparent PETG filament](https://www.amazon.de/-/en/Midori%C2%AE-filament-printer-transparent-tangle-free/dp/B08LNVYB1Q)
- Arduino Uno or similiar to flash the Attiny
- 1x [Attiny85](https://www.reichelt.de/8-bit-attiny-avr-risc-mikrocontroller-1-kb-20-mhz-dip-8-attiny-13a-pu-p117843.html) 
- 36 LEDs/0.25m of [WS2812B LED (144 LEDs/m)](https://www.ebay.de/itm/174974440899)
- 2x [M2x10mm self-tapping screws](https://www.ebay.de/itm/153270534010)
- 1x [8mm aluminium tube](https://www.hornbach.de/shop/Rundrohr-Aluminium-8x1-mm-1-m/8829150/artikel.html)
- optional: 1x [Attiny sockets](https://www.amazon.de/-/en/gp/product/B01N27VOIE/ref=ppx_yo_dt_b_search_asin_title)
- optional: 1x [Waterproof 5V Voltage regulator](https://www.amazon.de/-/en/gp/product/B07WSWMNN8/ref=ppx_yo_dt_b_search_asin_title)

## Getting started

The overall concept is pretty simple: The Attiny is hooked up to the digital LEDs
and turn the front facing ones white and the rear ones red. Everything is powered
with 5V and that's basically it.
The overall power consumption amounts to around 4W.

## 3D Printed parts

You can find the 3D printed parts in the 3dprint/. You will basically need the
interior part in any color you like and the external part (cover) in transparent.

## Setting up the electronics

Look up how to flash Attiny85 with Arduino as ISP [here](https://makersportal.com/blog/2018/5/19/attiny85-arduino-board-how-to-flash-the-arduino-bootloader-and-run-a-simple-sketch).

If that is done, flash ridesafe-flag.ino onto your Attiny.

The data line of the LEDs need to be connected to pin 6 on the Attiny85.
The LEDs data line is unidirectional, that is usually marked with a small arrow
on the LED-strip, so make sure that these are correctly oriented.

![](https://cdn.sparkfun.com/r/600-600/assets/f/8/f/d/9/52713d5b757b7fc0658b4567.png)

The Attiny and LED strip needs to be connected to 5V power supply and that's
all of the wiring already.

The sketch is designed to work with a string of 36 LEDs separated into a white/red part with each 18 LEDs.
The strip needs to be separated into two 18 LED parts which are connected at the top:

![](doc/images/split.jpg)

## Assembling The Parts

The hole for the 8mm aluminium tube is intended to be drilled first to provide a perfect fit.
At the end of the hole is a stop to prevent the tube from sliding up too far,
so make sure to not remove that.
After drilling the hole to size, insert the tube up to the stop. If it slides in
too easy you should glue it in.

![](doc/images/interior-wireframe.png)

Attach the LED strips using double sided adhesive tape on the front an back side of the flag.
The Attiny can be hold in place using a small strip of tape.

The cable for power supply is run inside the aluminium tube. Up to this point I
was too lazy to design a proper mount for the tube, so.... zip ties to the rescue!

\-			   | \-
:-------------------------:|:-------------------------:
![](doc/images/inside.jpg)  |  ![](doc/images/zip-ties.jpg)

Finally slide on the transparent cover and secure it with two screws:

![](doc/images/top-screws.jpg)


## Possible Improvements

- try [RGBNW LEDs](https://www.velomobilforum.de/forum/index.php?threads/ws2812b-led-fahne.68096/#post-1508053) for lower power consumption/more brightness

## Future Goals

Right now the flag is pretty dumb by just showing the same colors when powered on.
As there's already a microcontroller in the flag it could serve more than one
purpose, i.e. blinking red when braking or something like that.

Also, the same electronics could be build into other components like turn signals.

All of these components could be connected with I2C or something similiar and
being controlled by a "master" which includes switches/connector to control each
function (turn signal, switch on the brake lever, etc.) resulting in a fully
functional lighting system.
